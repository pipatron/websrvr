/* websrvr 0.1
Theory of operation:
- Starts from inetd, tcp connection on stdin/stdout
- mode1: started without arguments: output http headers + a default html message.
- mode2: started with 1 argument:
 argument is a filename; load it. Figure out mimetype from fileextention.
 output http headers(including mimetype) + file data.
- mode3: started with 2 arguments:
 argument 1 is filename; load it. argument 2 is custom mimetype.
 output http headers(including mimetype) + file data.

 TODO:  vilka headers är obligatoriska? Date? Last-Modified? Content-Length?
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>


/*
    This returns the mime-type of a filename based on the extension.
*/
static const char *get_mime( const char *filename )
{
    /*
        Table with extension -> mime type mapping
        
        "static const" means that it will not take any room on the stack,
        it will be statically compiled into the binary.
        
        "[]" means that it is an array with a size defined by the declaration.
        We calculate the size later by checking how many bytes the whole table takes,
        divided by the size of a single entry.
    */
    static const struct {
        const char *ext;
        const char *mime;
    } map[] = {
        { "html", "text/html" },
        { "txt", "text/plain" },
        { "jpg", "image/jpeg" },
        { "png", "image/png" },
        { "gif", "image/gif" },
        { "svg", "image/svg+xml" }
    };
    const size_t nmime = sizeof(map)/sizeof(*map);

    /* Search for the last dot in the filename */
    const char *ext = strrchr( filename, '.' );

    if( ext != NULL )
    {
        size_t i;

        /* We can't 'switch' on a string, so we must do string comparisons.
         ext points to the dot, ext+1 means look *after* the dot */
        for( i=0; i<nmime; ++i )
            if( strcmp( ext+1, map[i].ext ) == 0 )
                return map[i].mime;
    }
            
    /* If nothing matches, return the default. */
    return "application/octet-stream";
}

/*
    Tries to read a file to a buffer, and returns it. If the file does not exist, return NULL.
    The size of the buffer is written to "filesize" if supplied.
    The buffer must be free()d.
    If no memory can be allocated, this returns NULL.
*/
static char *get_file( const char *filename, size_t *filesize )
{
    FILE *fh;
    char *buf;
    long size;

    assert( filename != NULL );
    
    /* Open a file for reading, binary */
    fh = fopen( filename, "rb" );
    
    if( fh == NULL )
    {
        perror( filename );
        return NULL;
    }
 
    /* Find the length of the file using standard C routines. */
    fseek( fh, 0, SEEK_END );
    size = ftell( fh );
    fseek( fh, 0, SEEK_SET );

    buf = malloc( size );
 
    if( buf == NULL )
    {
        perror( NULL );
        return NULL;
    }
    
    /* TODO: Lots of error checking can be added here! */    
    fread( buf, size, 1, fh );
    fclose( fh );
    
    /* Copy the size to the argument, only if it is a valid pointer */
    if( filesize != NULL )
        *filesize = size;
        
    return buf;
}

/* Print a common status header with custom status */
void print_common( int status, const char *message )
{
    printf( "HTTP/1.0 %d %s\r\n", status, message );
    printf( "Server: websrvr 0.1\r\n" );

    /* If the status is not 200, we will simply stop here. */
    if( status != 200 )
    {
        printf( "\r\n" );
        exit( 0 );
    }
}

/* Print the normal "ok" headers */
void print_header( size_t size, const char *mime )
{
    print_common( 200, "OK" );
    printf( "Content-Type: %s\r\n", mime );
    printf( "Content-Length: %lu\r\n", (unsigned long)size );
    /* one blank line after the last http-header */
    printf( "\r\n" );
}

/* Not a string, an array */
static const char missing[] = "<html><body><h2>Server temporarily down</h2><p>(no reason set by admin)</body></html>\r\n";

int main( int argc, char *argv[] )
{
    /*
     * fixa: vänta på GET  (och timeout på det..?)
     * read() + strcmp()  ?
     */
    char *buf = (char *)missing;
    size_t size = sizeof(missing) - 1;
    char request[8192];
    const char *mime;

    /* Different options depending on the argument count */
    switch( argc )
    {
        case 1:
            mime = "text/html";
            break;
        case 2:
            mime = get_mime( argv[1] );
            break;
        case 3:
            mime = argv[2];
            break;
        default:
            fprintf( stderr, "Usage error: needs 0 to 2 arguments.\n" );
            exit( EXIT_FAILURE );
    }

    /* Now wait for the actual request */
    if( NULL == fgets( request, sizeof(request), stdin ) )
        print_common( 400, "Bad Request" );

    /* TODO: Should we even care about lowercase? */
    if( strncmp( "GET ", request, 4 ) && strncmp( "HEAD ", request, 5 ) )
        print_common( 405, "Method Not Allowed" );

    /* See if we have a file */
    if( argc > 1 )
    {
        buf = get_file( argv[1], &size );
        if( buf == NULL )
            print_common( 500, "Internal Server Error" );
    }

    /* Always send a header, for GET and HEAD */
    print_header( size, mime );

    /* Only send the file if it's an actual 'GET' */
    if( request[0] == 'G' )
        fwrite( buf, size, 1, stdout );

    return EXIT_SUCCESS;
}

