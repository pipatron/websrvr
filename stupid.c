/* A stupid simple web server. Usage: call "stupid <filename>" from inetd. */
#include <stdio.h>
#include <unistd.h>
int main( __attribute__((unused)) int argc, char *argv[] )
{
    scanf( "GET %*s HTTP/1.%*[01]\r\n" ); /* Actually ignores everything     */
    printf( "HTTP/1.0 200 OK\r\n\r\n" );  /* Almost-minimal header           */
    return execv( "/bin/cat", argv );     /* Have cat dump the file and quit */
}
